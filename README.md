# Back-end-Authentication-Service

Part of project BSS/OSS suite.

## Start gRPC AuthService Server

```shell
go run main.go
```

## Test from command-line

```shell
grpcurl -plaintext -d '{"username": "your-username", "password": "your-password"}' localhost:50051 AuthService/Login
```

output:

```shell
2023/10/31 14:38:21 INFO: [core] [Channel #1] Channel created
2023/10/31 14:38:21 INFO: [core] [Channel #1] original dial target is: "localhost:50051"
2023/10/31 14:38:21 INFO: [core] [Channel #1] parsed dial target is: {URL:{Scheme:localhost Opaque:50051 User: Host: Path: RawPath: OmitHost:false ForceQuery:false RawQuery: Fragment: RawFragment:}}
2023/10/31 14:38:21 INFO: [core] [Channel #1] fallback to scheme "passthrough"
2023/10/31 14:38:21 INFO: [core] [Channel #1] parsed dial target is: {URL:{Scheme:passthrough Opaque: User: Host: Path:/localhost:50051 RawPath: OmitHost:false ForceQuery:false RawQuery: Fragment: RawFragment:}}
2023/10/31 14:38:21 INFO: [core] [Channel #1] Channel authority set to "localhost:50051"
2023/10/31 14:38:21 INFO: [core] [Channel #1] Resolver state updated: {
  "Addresses": [
    {
      "Addr": "localhost:50051",
      "ServerName": "",
      "Attributes": null,
      "BalancerAttributes": null,
      "Type": 0,
      "Metadata": null
    }
  ],
  "ServiceConfig": null,
  "Attributes": null
} (resolver returned new addresses)
2023/10/31 14:38:21 INFO: [core] [Channel #1] Channel switches to new LB policy "pick_first"
2023/10/31 14:38:21 INFO: [core] [Channel #1 SubChannel #2] Subchannel created
2023/10/31 14:38:21 INFO: [core] [Channel #1] Channel Connectivity change to CONNECTING
2023/10/31 14:38:21 INFO: [core] [Channel #1 SubChannel #2] Subchannel Connectivity change to CONNECTING
2023/10/31 14:38:21 INFO: [core] [Channel #1 SubChannel #2] Subchannel picks a new address "localhost:50051" to connect
2023/10/31 14:38:21 INFO: [core] pickfirstBalancer: UpdateSubConnState: 0x1400011d4d0, {CONNECTING <nil>}
2023/10/31 14:38:22 INFO: [core] [Channel #1 SubChannel #2] Subchannel Connectivity change to READY
2023/10/31 14:38:22 INFO: [core] pickfirstBalancer: UpdateSubConnState: 0x1400011d4d0, {READY <nil>}
2023/10/31 14:38:22 INFO: [core] [Channel #1] Channel Connectivity change to READY
{
  "token": "dummy-token"
}
2023/10/31 14:38:22 INFO: [core] [Channel #1] Channel Connectivity change to SHUTDOWN
2023/10/31 14:38:22 INFO: [core] [Channel #1] ccBalancerWrapper: closing
2023/10/31 14:38:22 INFO: [core] [Channel #1] Closing the name resolver
2023/10/31 14:38:22 INFO: [core] [Channel #1 SubChannel #2] Subchannel Connectivity change to SHUTDOWN
2023/10/31 14:38:22 INFO: [core] [Channel #1 SubChannel #2] Subchannel deleted
2023/10/31 14:38:22 INFO: [core] [Channel #1] Channel deleted
```